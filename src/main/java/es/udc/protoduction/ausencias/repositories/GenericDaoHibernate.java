/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.repositories;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author david
 */
@Transactional
public class GenericDaoHibernate<T, PK extends Serializable> implements GenericDao<T, PK> {

    @Autowired
    private SessionFactory sessionFactory;
    private final Class<T> entityClass;

    public GenericDaoHibernate() {
        entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    protected Criteria findByAttribute(String attribute, Object value) {
        return getSession().createCriteria(entityClass)
                .add(Restrictions.eq(attribute, value));
    }

    protected Criteria findByAttributes(Map<String, Object> attributes) {
        Criteria criteria = getSession().createCriteria(entityClass);
        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
            criteria.add(Restrictions.eq(entry.getKey(), entry.getValue()));
        }
        return criteria;
    }

    public void create(T t) {
        getSession().save(t);
    }

    public void update(T t) {
        getSession().update(t);
    }

    public void save(T t) {
        getSession().saveOrUpdate(t);
    }

    public void delete(T t) {
        getSession().delete(t);
    }

    public T find(PK o) {
        return (T) getSession().get(entityClass, o);
    }

    public List<T> findAll() {
        return getSession().createCriteria(entityClass).list();
    }

}
