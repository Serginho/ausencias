DELETE FROM Proyecto_Trabajador;
DELETE FROM Ausencia;
DELETE FROM Proyecto;
DELETE FROM Trabajador;

INSERT INTO Trabajador (Id, Name, Surname, Remaining_holidays, Enjoyed_holidays, Ending_date_contract)
               VALUES (1, 'Paco', 'Garcia', 10, 5, {ts '2015-09-17 18:47:52.69'});
INSERT INTO Trabajador (Id, Name, Surname, Remaining_holidays, Enjoyed_holidays, Ending_date_contract)
               VALUES (2, 'Manolo', 'Cabesa Huevo', 15, 7, {ts '2017-08-15 18:47:52.69'});
INSERT INTO Trabajador (Id, Name, Surname, Remaining_holidays, Enjoyed_holidays, Ending_date_contract)
               VALUES (3, 'Julia', 'Souto', 22, 7, {ts '2017-08-15 18:47:52.69'});
INSERT INTO Trabajador (Id, Name, Surname, Remaining_holidays, Enjoyed_holidays, Ending_date_contract)
               VALUES (4, 'Roland', 'Deschain', 20, 7, {ts '2017-08-15 18:47:52.69'});
INSERT INTO Trabajador (Id, Name, Surname, Remaining_holidays, Enjoyed_holidays, Ending_date_contract)
               VALUES (5, 'Nino', 'Bravo', 4, 1, {ts '2017-08-15 18:47:52.69'});

INSERT INTO Proyecto (Id, Nombre, Fecha_fin, Responsable, Personal_minimo)
               VALUES(1, 'Protoduction', {ts '2016-12-25 18:47:52.69'}, 2, 1);
INSERT INTO Proyecto (Id, Nombre, Fecha_fin, Responsable, Personal_minimo)
               VALUES(2, 'Ausencias', {ts '2014-08-15 18:47:52.69'}, 2, 1);

INSERT INTO Proyecto_Trabajador (Proyecto_id, Trabajador_id) VALUES (1,1);
INSERT INTO Proyecto_Trabajador(Proyecto_id, Trabajador_id) VALUES (2,1);
INSERT INTO Proyecto_Trabajador (Proyecto_id, Trabajador_id) VALUES (1,2);
INSERT INTO Proyecto_Trabajador(Proyecto_id, Trabajador_id) VALUES (2,2);