package es.udc.protoduction.ausencias.repositories;

import es.udc.protoduction.ausencias.entities.Trabajador;
import org.springframework.stereotype.Repository;

@Repository("trabajadorDao")
public class TrabajadorDaoImpl extends GenericDaoHibernate<Trabajador, Long>
        implements TrabajadorDao {

}
