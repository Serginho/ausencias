package es.udc.protoduction.ausencias.repositories;

import es.udc.protoduction.ausencias.entities.Trabajador;

/**
 *
 * @author david
 */
public interface TrabajadorDao extends GenericDao<Trabajador, Long> {

}
