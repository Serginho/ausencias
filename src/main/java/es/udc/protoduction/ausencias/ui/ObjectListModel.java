package es.udc.protoduction.ausencias.ui;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author david
 */
public class ObjectListModel<T> extends AbstractListModel<String> {

    private List<T> objects = new ArrayList<>();
    private final Method getter;

    /**
     * Constructor por defecto
     *
     * @param getter Nombre del método que se llama sobre T para obtener el
     * objeto que se mostrará en la vista. Ej: "getName" sobre Trabajador.
     */
    public ObjectListModel(Method getter) {
        this.getter = getter;
    }

    @Override
    public int getSize() {
        return objects.size();
    }

    @Override
    public String getElementAt(int index) {
        try {
            return getter.invoke(objects.get(index)).toString();
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void addElement(T element) {
        objects.add(element);
        fireIntervalAdded(this, this.getSize(), this.getSize());
    }

    public void clear() {
        if (this.getSize() > 0) {
            objects = new ArrayList<>(); //Un borrado aquí borraría el modelo
            fireIntervalRemoved(this, 0, this.getSize());
        }
    }

    public T get(int index) {
        return objects.get(index);
    }

    public void setObjects(List<T> objects) {
        this.objects = objects;
        fireIntervalAdded(this, 0, this.getSize());
    }
}
