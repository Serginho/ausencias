/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.udc.protoduction.ausencias.services.exceptions;

import es.udc.protoduction.ausencias.entities.Ausencia;

/**
 *
 * @author diana
 */
public class RechazoNoValidoException extends Exception {
    
    Ausencia ausencia;

    public RechazoNoValidoException(Ausencia ausencia) {
        this.ausencia = ausencia;
    }
    
    public RechazoNoValidoException(Ausencia ausencia, String msg) {
        super(msg);
        this.ausencia = ausencia;
    }

    public Ausencia getAusencia() {
        return ausencia;
    }    
    
    
}