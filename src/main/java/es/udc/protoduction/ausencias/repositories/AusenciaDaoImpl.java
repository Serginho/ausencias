/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.udc.protoduction.ausencias.repositories;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.util.AusenciasState;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("ausenciaDao")
public class AusenciaDaoImpl extends GenericDaoHibernate<Ausencia, Long> implements AusenciaDao{
    
    private Session getSession() {
        return super.getSessionFactory().getCurrentSession();
    }
    
    @Override
    public List<Ausencia> findAllPending(){
        return getSession().createCriteria(Ausencia.class)
              .add(Restrictions.eq("estado", AusenciasState.PENDIENTE.getValue())).list();
    }
}
