/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.udc.protoduction.ausencias.util;

/**
 *
 * @author diana
 */
public enum AusenciasState {
    
    PENDIENTE((byte) 0),
    ACEPTADA((byte) 1),
    RECHAZADA((byte) 2),
    CANCELADA_POR_USUARIO((byte) 3);

    private final byte value;

    private AusenciasState(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return this.value;
    }

    public static AusenciasState valueOf(byte b) {
        for (AusenciasState iter : AusenciasState.values()) {
            if (b == iter.getValue()) {
                return iter;
            }
        }
        throw new RuntimeException("The byte value: " + String.valueOf(b)
                + " does not match with any AusenciasState");
    }
    
}
