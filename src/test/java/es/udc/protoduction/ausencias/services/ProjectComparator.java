package es.udc.protoduction.ausencias.services;

import java.util.Comparator;

import es.udc.protoduction.ausencias.entities.Proyecto;

public class ProjectComparator implements Comparator<Proyecto> {

	@Override
	public int compare(Proyecto o1, Proyecto o2) {
		return o1.getName().compareTo(o2.getName());
	}

}
