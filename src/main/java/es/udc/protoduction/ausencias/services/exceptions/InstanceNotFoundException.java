package es.udc.protoduction.ausencias.services.exceptions;

/**
 *
 * @author david
 */
public class InstanceNotFoundException extends Exception {

    private Object key;
    private String className;

    public InstanceNotFoundException(Object key, String className) {
        super("Instance of class: " + className + " not found, key: " + key.toString());
    }

    public Object getKey() {
        return key;
    }

    public String getClassName() {
        return className;
    }

}
