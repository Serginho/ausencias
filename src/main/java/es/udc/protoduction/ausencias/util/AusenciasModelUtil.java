/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.util;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.services.exceptions.AusenciaNoValidaException;
import java.util.Date;

/**
 *
 * @author diana
 */
public class AusenciasModelUtil {

    public static final long ONE_DAY = 1000 * 60 * 60 * 24;

    public static void verificarAusencia(Ausencia a) throws AusenciaNoValidaException {

        long fechaInicio = a.getFechaIni().getTimeInMillis();
        long fechaFin = a.getFechaFin().getTimeInMillis();

        if (fechaFin < fechaInicio) {
            throw new AusenciaNoValidaException(a, "La fecha fin debe ser posterior a la fecha de inicio");
        }
        if (fechaInicio < ((new Date()).getTime())) {
            throw new AusenciaNoValidaException(a, "La fecha de inicio debe ser posterior a la actual");
        }
        if (a.getTipoAusencia() != AusenciasType.BAJA.getValue()) {
            long dias = (fechaFin - fechaInicio) / ONE_DAY;

            if (dias > a.getTrabajador().getRemainingHolidays()) {
                throw new AusenciaNoValidaException(a, "El trabajador no tiene suficientes dias de vacaciones");
            }
        }
    }
}
