/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.udc.protoduction.ausencias.repositories;

import es.udc.protoduction.ausencias.entities.Ausencia;
import java.util.List;

/**
 *
 * @author gabriel
 */
public interface AusenciaDao extends GenericDao<Ausencia, Long>{
    
    public List<Ausencia> findAllPending();
    
}
