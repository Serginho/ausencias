package es.udc.protoduction.ausencias.entities;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

@Entity
public class Proyecto {

    private long id;
    private String name;
    private Calendar endDate;
    private Trabajador responsible;
    private int minimalWorkers;
    private Set<Trabajador> workers = new HashSet<>();

    public Proyecto() {
    }

    public Proyecto(long id, String name, Calendar endDate,
            Trabajador responsible, int minimalWorkers) {
        this.id = id;
        this.name = name;
        this.endDate = endDate;
        this.responsible = responsible;
        this.minimalWorkers = minimalWorkers;
    }

    @Column(name = "Id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToMany(mappedBy = "projects", fetch = FetchType.LAZY)
    public Set<Trabajador> getWorkers() {
        return workers;
    }

    public void setWorkers(Set<Trabajador> workers) {
        this.workers = workers;
    }

    @Column(name = "Nombre")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Fecha_fin")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    @OneToOne
    @JoinColumn(name = "Responsable")
    public Trabajador getResponsible() {
        return responsible;
    }

    public void setResponsible(Trabajador responsible) {
        this.responsible = responsible;
    }

    @Column(name = "Personal_minimo")
    public int getMinimalWorkers() {
        return minimalWorkers;
    }

    public void setMinimalWorkers(int minimalWorkers) {
        this.minimalWorkers = minimalWorkers;
    }

    @Override
    public String toString() {
        return name;
    }

    public void addWorker(Trabajador t) {
        this.getWorkers().add(t);
        t.getProjects().add(this);
    }

    public void removeWorker(Trabajador t) {
        this.getWorkers().remove(t);
        t.getProjects().remove(this);
    }
}
