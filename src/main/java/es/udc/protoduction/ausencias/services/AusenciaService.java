/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.services;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.services.exceptions.AusenciaNoValidaException;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import es.udc.protoduction.ausencias.services.exceptions.RechazoNoValidoException;
import es.udc.protoduction.ausencias.util.AusenciasState;
import es.udc.protoduction.ausencias.util.AusenciasType;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author diana.yanez
 */
public interface AusenciaService {

    public Ausencia saveAusencia(Calendar fechaIni, Calendar fechaFin, AusenciasType tipo, AusenciasState estado, String content, long workerId) throws AusenciaNoValidaException, InstanceNotFoundException;

    public List<Ausencia> findAll();

    public List<Ausencia> findAllPending();

    public List<Ausencia> findByWorkerId(long workerId) throws InstanceNotFoundException;

    /**
     *
     * @param idAus
     * @return String vacio si la ausencia no rompe restricciones de numero
     * minimo de trabajadores, la lista de proyectos en que lso rompe en caso
     * contrario
     */
    public List<Proyecto> esValida(long idAus) throws InstanceNotFoundException;

    public List<Ausencia> findAcceptedByWorkerId(long workerId) throws InstanceNotFoundException;

    public Ausencia cancelAusencia(long ausenciaId) throws InstanceNotFoundException;

    public void updateAusencia(Ausencia a);

    public void rechazarAusencia(Ausencia ausencia, String motivo) throws RechazoNoValidoException;
}
