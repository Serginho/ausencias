/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.repositories;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author david
 */
public interface GenericDao<T, PK extends Serializable> {

    public void create(T t);

    public void update(T t);

    public void save(T t);

    public void delete(T t);

    public T find(PK o);

    public List<T> findAll();
}
