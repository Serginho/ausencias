/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.ui;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.services.AusenciaService;
import es.udc.protoduction.ausencias.services.TrabajadorService;
import es.udc.protoduction.ausencias.services.exceptions.AusenciaNoValidaException;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import es.udc.protoduction.ausencias.util.AusenciasState;
import es.udc.protoduction.ausencias.util.AusenciasType;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kaworu
 */
@Component("CrearSolicitud")
public class CrearSolicitud extends javax.swing.JFrame {

    /**
     * Creates new form CrearSolicitud2
     */
    private long userId = 1;
    @Autowired
    AusenciaService ausenciaService;
    @Autowired
    TrabajadorService trabajadorService;

    /**
     * Creates new form CrearSolicitud
     */
    public CrearSolicitud() {
        this.setTitle("Crear ausencia");
        this.initComponents();
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                EmployeeUI.crearSolicitudOpened = false;
            }
        });
    }

    public void init() {
        setLabel();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        newAusenciaForm = new javax.swing.JPanel();
        buttonAceptar = new javax.swing.JButton();
        buttonResetear = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tareaMotivo = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        comboTipos = new javax.swing.JComboBox();
        calendarFIni = new com.toedter.calendar.JCalendar();
        calendarFFin = new com.toedter.calendar.JCalendar();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblDias = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        newAusenciaForm.setEnabled(false);

        buttonAceptar.setText("Aceptar");
        buttonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAceptarActionPerformed(evt);
            }
        });

        buttonResetear.setText("Resetear");
        buttonResetear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonResetearActionPerformed(evt);
            }
        });

        tareaMotivo.setColumns(20);
        tareaMotivo.setRows(5);
        jScrollPane3.setViewportView(tareaMotivo);

        jLabel5.setText("Motivo");

        jLabel6.setText("Tipo");

        comboTipos.setModel(new javax.swing.DefaultComboBoxModel(AusenciasType.values()));

        calendarFIni.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                calendarFIniMouseClicked(evt);
            }
        });

        jLabel1.setText("Fecha inicio");

        jLabel2.setText("Fecha fin");

        javax.swing.GroupLayout newAusenciaFormLayout = new javax.swing.GroupLayout(newAusenciaForm);
        newAusenciaForm.setLayout(newAusenciaFormLayout);
        newAusenciaFormLayout.setHorizontalGroup(
            newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(newAusenciaFormLayout.createSequentialGroup()
                .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(newAusenciaFormLayout.createSequentialGroup()
                        .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(newAusenciaFormLayout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel1)
                                .addGap(147, 147, 147))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, newAusenciaFormLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(calendarFIni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)
                        .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(calendarFFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(33, 33, 33)
                        .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(newAusenciaFormLayout.createSequentialGroup()
                                .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))
                                .addGap(26, 26, 26)
                                .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane3)
                                    .addComponent(comboTipos, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(lblDias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(newAusenciaFormLayout.createSequentialGroup()
                        .addGap(220, 220, 220)
                        .addComponent(buttonAceptar)
                        .addGap(60, 60, 60)
                        .addComponent(buttonResetear)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        newAusenciaFormLayout.setVerticalGroup(
            newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(newAusenciaFormLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDias, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(calendarFIni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(calendarFFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(newAusenciaFormLayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(comboTipos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addGroup(newAusenciaFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonAceptar)
                    .addComponent(buttonResetear))
                .addGap(27, 27, 27))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(newAusenciaForm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(newAusenciaForm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAceptarActionPerformed
        Calendar fIni = this.calendarFIni.getCalendar();
        Calendar fFin = this.calendarFFin.getCalendar();
        Calendar now = Calendar.getInstance();
        String motivo = this.tareaMotivo.getText();
        AusenciasType tipo = (AusenciasType) this.comboTipos.getSelectedItem();

        try {
            Ausencia a = ausenciaService.saveAusencia(fIni, fFin, tipo, AusenciasState.PENDIENTE, motivo, userId);
            this.newAusenciaForm.setEnabled(false);
            JOptionPane.showMessageDialog(this, "Ausencia creada  ", "Información", JOptionPane.INFORMATION_MESSAGE);
            setLabel();
        } catch (InstanceNotFoundException ex) {
            Logger.getLogger(CrearSolicitud.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AusenciaNoValidaException ex) {
            this.newAusenciaForm.setEnabled(false);
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Ausencia no creada", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_buttonAceptarActionPerformed

    private void buttonResetearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonResetearActionPerformed
        this.calendarFFin.setCalendar(Calendar.getInstance());
        this.calendarFIni.setCalendar(Calendar.getInstance());
        this.tareaMotivo.setText("");
        this.comboTipos.setSelectedIndex(0);
    }//GEN-LAST:event_buttonResetearActionPerformed

    private void calendarFIniMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_calendarFIniMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_calendarFIniMouseClicked

    private void setLabel() {
        this.lblDias.setText("Te quedan " + String.valueOf(((Trabajador) trabajadorService.findById(userId)).getRemainingHolidays()) + " días de vacaciones");
        //this.lblDias.setText("Te quedan " + "12" + " d�as de vacaciones");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CrearSolicitud.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CrearSolicitud.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CrearSolicitud.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CrearSolicitud.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
                CrearSolicitud crearSolicitud = context.getBean(CrearSolicitud.class);
                crearSolicitud.setLabel();
                crearSolicitud.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAceptar;
    private javax.swing.JButton buttonResetear;
    private com.toedter.calendar.JCalendar calendarFFin;
    private com.toedter.calendar.JCalendar calendarFIni;
    private javax.swing.JComboBox comboTipos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblDias;
    private javax.swing.JPanel newAusenciaForm;
    private javax.swing.JTextArea tareaMotivo;
    // End of variables declaration//GEN-END:variables

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
