package es.udc.protoduction.ausencias.repositories;

import es.udc.protoduction.ausencias.entities.Proyecto;

/**
 *
 * @author serginho
 */
public interface ProyectoDao extends GenericDao<Proyecto, Long> {
}
