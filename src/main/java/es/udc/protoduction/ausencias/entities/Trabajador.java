package es.udc.protoduction.ausencias.entities;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class Trabajador {

    private long id;
    private String name;
    private String surname;
    private int remainingHolidays;
    private int enjoyedHolidays;
    private Calendar endingDateContract;
    private Set<Ausencia> ausencias = new HashSet<>();
    private Set<Proyecto> projects = new HashSet<>();

    public Trabajador() {
    }

    public Trabajador(long id, String name, String surname,
            int remainingHolidays, int enjoyedHolidays,
            Calendar endingDateContract) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.remainingHolidays = remainingHolidays;
        this.enjoyedHolidays = enjoyedHolidays;
        this.endingDateContract = endingDateContract;
    }

    @Column(name = "Id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToMany
    @JoinTable(name = "Proyecto_Trabajador", joinColumns = {
        @JoinColumn(name = "Trabajador_id")}, inverseJoinColumns = {
        @JoinColumn(name = "Proyecto_id")})
    public Set<Proyecto> getProjects() {
        return projects;
    }

    public void setProjects(Set<Proyecto> projects) {
        this.projects = projects;
    }

    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "Remaining_holidays")
    public int getRemainingHolidays() {
        return remainingHolidays;
    }

    public void setRemainingHolidays(int remainingHolidays) {
        this.remainingHolidays = remainingHolidays;
    }

    @Column(name = "Enjoyed_holidays")
    public int getEnjoyedHolidays() {
        return enjoyedHolidays;
    }

    public void setEnjoyedHolidays(int enjoyedHolidays) {
        this.enjoyedHolidays = enjoyedHolidays;
    }

    @Column(name = "Ending_date_contract")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Calendar getEndingDateContract() {
        return endingDateContract;
    }

    public void setEndingDateContract(Calendar endingDateContract) {
        this.endingDateContract = endingDateContract;
    }

    @OneToMany(mappedBy = "trabajador", fetch = FetchType.LAZY)
    public Set<Ausencia> getAusencias() {
        return ausencias;
    }

    public void setAusencias(Set<Ausencia> ausencias) {
        this.ausencias = ausencias;
    }

    @Override
    public String toString() {
        return name;
    }

    public void addProject(Proyecto p) {
        this.getProjects().add(p);
        p.getWorkers().add(this);
    }

    public void removeProject(Proyecto p) {
        this.getProjects().remove(p);
        p.getWorkers().remove(this);
    }
}
