package es.udc.protoduction.ausencias.repositories;

import es.udc.protoduction.ausencias.entities.Proyecto;
import org.springframework.stereotype.Repository;

/**
 *
 * @author serginho
 */
@Repository("proyectoDao")
public class ProyectoDaoImpl extends GenericDaoHibernate<Proyecto, Long>
        implements ProyectoDao {
}
