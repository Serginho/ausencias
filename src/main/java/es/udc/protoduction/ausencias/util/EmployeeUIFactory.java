package es.udc.protoduction.ausencias.util;

import es.udc.protoduction.ausencias.ui.AssignedProjects;
import es.udc.protoduction.ausencias.ui.CrearSolicitud;
import es.udc.protoduction.ausencias.ui.EmployeeUI;
import es.udc.protoduction.ausencias.ui.VerAusencias;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author david
 */
public class EmployeeUIFactory {

    private static ApplicationContext context;
    private static EmployeeUI employeeUI;
    private static AssignedProjects assignedProjects;
    private static CrearSolicitud crearSolicitud;
    private static VerAusencias verAusencias;

    private static ApplicationContext getContext() {
        if (context == null) {
            context = new ClassPathXmlApplicationContext("spring-config.xml");
        }
        return context;
    }

    public static EmployeeUI getEmployeeUI() {
        if (employeeUI == null) {
            employeeUI = getContext().getBean(EmployeeUI.class);
        }
        return employeeUI;
    }

    public static AssignedProjects getAssignedProjects() {
        if (assignedProjects == null) {
            assignedProjects = getContext().getBean(AssignedProjects.class);
        }
        return assignedProjects;
    }

    public static CrearSolicitud getCrearSolicitud() {
        if (crearSolicitud == null) {
            crearSolicitud = getContext().getBean(CrearSolicitud.class);
        }
        return crearSolicitud;
    }

    public static VerAusencias getVerAusencias() {
        if (verAusencias == null) {
            verAusencias = getContext().getBean(VerAusencias.class);
        }
        return verAusencias;
    }
}
