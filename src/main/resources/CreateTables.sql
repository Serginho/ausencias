DROP TABLE IF EXISTS Ausencia;
DROP TABLE IF EXISTS Proyecto_Trabajador;
DROP TABLE IF EXISTS Trabajador;
DROP TABLE IF EXISTS Proyecto;

CREATE TABLE IF NOT EXISTS Trabajador 
(
    Id BIGINT NOT NULL AUTO_INCREMENT,
    Name VARCHAR(255) NOT NULL,
    Surname VARCHAR(255) NOT NULL,
    Remaining_holidays INTEGER NOT NULL,
    Enjoyed_holidays INTEGER NOT NULL, 
    Ending_date_contract TIMESTAMP NOT NULL,
    CONSTRAINT PK_Trabajador PRIMARY KEY (Id)
);

CREATE TABLE IF NOT EXISTS Ausencia 
(
    Id BIGINT NOT NULL AUTO_INCREMENT,
    Descripcion VARCHAR(255) NOT NULL,
    Motivo VARCHAR(255),
    Fecha_ini TIMESTAMP NOT NULL,
    Fecha_fin TIMESTAMP NOT NULL,
    Tipo_ausencia TINYINT NOT NULL, 
    Trabajador_id INTEGER NOT NULL,
    Estado TINYINT NOT NULL,
    MotivoRechazo VARCHAR(255) NOT NULL,
    Vista BOOLEAN NOT NULL,
    CONSTRAINT PK_Ausencia PRIMARY KEY (Id),
    CONSTRAINT FK_Ausencia_trabajador FOREIGN KEY (Trabajador_id) REFERENCES Trabajador(Id)
);

CREATE TABLE IF NOT EXISTS Proyecto
(
	Id BIGINT NOT NULL AUTO_INCREMENT,
	Nombre VARCHAR(255) NOT NULL,
	Fecha_fin TIMESTAMP NOT NULL,
	Responsable BIGINT NOT NULL,
	Personal_minimo INTEGER NOT NULL,
	CONSTRAINT PK_Proyecto PRIMARY KEY (Id),
	CONSTRAINT FK_Proyecto_responsable FOREIGN KEY (Responsable) REFERENCES Trabajador(Id) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS Proyecto_Trabajador
(
	Proyecto_id BIGINT NOT NULL,
	Trabajador_id BIGINT NOT NULL,
	CONSTRAINT PK_Proyecto_Trabajador PRIMARY KEY (Proyecto_id, Trabajador_id),
	CONSTRAINT FK_Proyecto_Trabajador_Proyecto FOREIGN KEY (Proyecto_id) REFERENCES Proyecto(Id) ON DELETE CASCADE,
	CONSTRAINT FK_Proyecto_Trabajador_Trabajador FOREIGN KEY (Trabajador_id) REFERENCES Trabajador(Id) ON DELETE CASCADE
);

