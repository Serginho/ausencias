/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.ui;

import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.services.TrabajadorService;
import java.util.List;
import javax.swing.AbstractListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author gabriel
 */

@Component("trabajadorModel")
public class TrabajadoresModel extends AbstractListModel<String> {

    @Autowired
    TrabajadorService trabajadorService;
    
    private List<Trabajador> trabajadores;
    
    @Override
    public int getSize() { 
        if (trabajadores == null)
            trabajadores = trabajadorService.findAll();
        return trabajadores.size(); }
            

    @Override
    public String getElementAt(int i) {
        if (trabajadores == null)
           trabajadores = trabajadorService.findAll();
        return trabajadores.get(i).getName();     }
    
    public Trabajador get(int i){
        return trabajadores.get(i);
    }
    
    public void actualizar(){
        trabajadores = trabajadorService.findAll();
        this.fireIntervalAdded(this, 0, trabajadores.size());
    }
    
}
