package es.udc.protoduction.ausencias.services;

import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.repositories.ProyectoDao;
import es.udc.protoduction.ausencias.repositories.TrabajadorDao;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml",
    "classpath:spring-config-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class ProjectServiceTest {

    @Autowired
    ProyectoService projectService;
    @Autowired
    private TrabajadorDao workerDao;
    @Autowired
    private ProyectoDao projectDao;

    @Test
    public void getProjectsByWorkerIdTest() throws InstanceNotFoundException {
        Trabajador t = new Trabajador(0, "pepe", "surname", 0, 0,
                Calendar.getInstance());
        workerDao.create(t);

        Proyecto p = new Proyecto(0, "p1", Calendar.getInstance(), t, 5);
        projectDao.create(p);
        Proyecto otherP = new Proyecto(0, "p2", Calendar.getInstance(), t, 5);
        projectDao.create(otherP);

        t.addProject(p);
        t.addProject(otherP);
        workerDao.update(t);

        List<Proyecto> expected = new ArrayList<>();
        expected.add(p);
        expected.add(otherP);
        Collections.sort(expected, new ProjectComparator());

        List<Proyecto> result = projectService.getProjectByWorkerId(t.getId());
        Collections.sort(result, new ProjectComparator());
        assertEquals(expected, result);
    }

    @Test
    public void setWorkers() throws InstanceNotFoundException {
        Trabajador pepe = new Trabajador(0, "pepe", "surname", 0, 0,
                Calendar.getInstance());
        workerDao.create(pepe);

        Trabajador paco = new Trabajador(0, "paco", "surname", 0, 0,
                Calendar.getInstance());
        workerDao.create(paco);

        Trabajador manolo = new Trabajador(0, "manolo", "surname", 0, 0,
                Calendar.getInstance());
        workerDao.create(manolo);

        // Responsable de proyecto, no interviene en el test
        Trabajador javier = new Trabajador(0, "Javier", "Andrade", 0, 0,
                Calendar.getInstance());
        workerDao.create(javier);

        Proyecto p = new Proyecto(0, "p1", Calendar.getInstance(), javier, 5);
        p.addWorker(pepe);
        p.addWorker(paco);
        projectDao.create(p);

        Set<Long> listaTrabajadores = new HashSet<>();
        listaTrabajadores.add(paco.getId());
        listaTrabajadores.add(manolo.getId());

        projectService.setWorkers(p.getId(), listaTrabajadores);

        Set<Trabajador> expected = new HashSet<>();
        expected.add(paco);
        expected.add(manolo);

        assertEquals(expected, p.getWorkers());

    }

}
