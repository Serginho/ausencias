/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.repositories.AusenciaDao;
import es.udc.protoduction.ausencias.repositories.ProyectoDao;
import es.udc.protoduction.ausencias.repositories.TrabajadorDao;
import es.udc.protoduction.ausencias.services.exceptions.AusenciaNoValidaException;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import es.udc.protoduction.ausencias.services.exceptions.RechazoNoValidoException;
import es.udc.protoduction.ausencias.util.AusenciasModelUtil;
import es.udc.protoduction.ausencias.util.AusenciasState;
import es.udc.protoduction.ausencias.util.AusenciasType;

/**
 * 
 * @author gabriel
 */
@Service("ausenciaService")
@Transactional
public class AusenciaServiceImpl implements AusenciaService {

	@Autowired
	AusenciaDao ausenciaDao;
	@Autowired
	TrabajadorDao trabajadorDao;
	@Autowired
	ProyectoDao proyectoDao;
	@Autowired
	TrabajadorService trabajadorService;

	@Override
	public Ausencia saveAusencia(Calendar fechaIni, Calendar fechaFin,
			AusenciasType tipo, AusenciasState estado, String content,
			long workerId) throws AusenciaNoValidaException,
			InstanceNotFoundException {
		Trabajador t = trabajadorDao.find(workerId);
		if (t == null) {
			throw new InstanceNotFoundException(workerId,
					Trabajador.class.getName());
		} else {
			Ausencia a = new Ausencia(0, content, fechaIni, fechaFin,
					tipo.getValue(), estado.getValue(), t);
			AusenciasModelUtil.verificarAusencia(a);
			ausenciaDao.create(a);
			t.getAusencias().add(a);
			trabajadorDao.update(t);
			return a;
		}
	}

	@Override
	public void updateAusencia(Ausencia a) {
		ausenciaDao.update(a);
	}

	@Override
	public List<Ausencia> findAll() {
		return ausenciaDao.findAll();
	}

	@Override
	public List<Ausencia> findAllPending() {
		return ausenciaDao.findAllPending();
	}

	@Override
	public List<Ausencia> findByWorkerId(long workerId)
			throws InstanceNotFoundException {
		Trabajador t = trabajadorDao.find(workerId);
		if (t == null) {
			throw new InstanceNotFoundException(t, Trabajador.class.getName());
		} else {
			return new ArrayList<>(t.getAusencias());
		}
	}

	@Override
	public Ausencia cancelAusencia(long ausenciaId)
			throws InstanceNotFoundException {
		Ausencia ausencia = ausenciaDao.find(ausenciaId);
		if (ausencia == null) {
			throw new InstanceNotFoundException(ausencia,
					Ausencia.class.getName());
		} else {
			ausencia.setEstado(AusenciasState.CANCELADA_POR_USUARIO.getValue());
			ausenciaDao.save(ausencia);
			return ausencia;
		}
	}

	@Override
	public List<Ausencia> findAcceptedByWorkerId(long workerId)
			throws InstanceNotFoundException {
		Trabajador t = trabajadorDao.find(workerId);
		if (t == null) {
			throw new InstanceNotFoundException(t, Trabajador.class.getName());
		} else {
			List<Ausencia> listaAux = new ArrayList<>();
			for (Ausencia aus : t.getAusencias()) {
				byte b = aus.getEstado();
				AusenciasState as = AusenciasState.valueOf(b);
				if (as.equals(AusenciasState.valueOf("ACEPTADA"))) {
					listaAux.add(aus);
				}
			}
			return listaAux;
		}
	}

	@Override
	public List<Proyecto> esValida(long idAus) throws InstanceNotFoundException {
		List<Proyecto> result = new ArrayList<>();
		Ausencia n = ausenciaDao.find(idAus);
		if (n == null)
			throw new InstanceNotFoundException(n, "Ausencia");
		Set<Proyecto> listaProyectos = n.getTrabajador().getProjects();
		int count;
		for (Proyecto p : listaProyectos) {
			count = 0;
			Set<Trabajador> workers = p.getWorkers();
			// Caso trivial: si solo hay un trabajador en el proyecto,
			// no podrá ausentarse a menos que el mínimo sea 0
			if (workers.size() == 1 && p.getMinimalWorkers() > 0) {
				result.add(p);
			} else {
				for (Trabajador t : workers) {
					// No comprobar conflictos con ausencias propias
					if (t.getId() != n.getTrabajador().getId()) {
						for (Ausencia a : t.getAusencias()) {
							if (a.getEstado() == AusenciasState.ACEPTADA
									.getValue()) {
								// n fin > a.ini && n.ini < a.fin
								if (n.getFechaFin().compareTo(a.getFechaIni()) >= 0
										&& n.getFechaIni().compareTo(
												a.getFechaFin()) <= 0) {
									count++;
									break;
								}
							}
						}
					}
				}
			}

			// Si ha habido al menos un conflicto se comprueba que el número
			// de trabajadores en el proyecto menos los que se ausentarían
			// sea menor que el mínimo, el -1 es el propio trabajador.
			if (count > 0 && workers.size() - count - 1 < p.getMinimalWorkers()) {
				result.add(p);
			}
		}
		return result;
	}

	@Override
	public void rechazarAusencia(Ausencia ausencia, String motivo)
			throws RechazoNoValidoException {
		if (ausencia.getTipoAusencia() != AusenciasType.BAJA.getValue()) {
			ausencia.setEstado(AusenciasState.RECHAZADA.getValue());
			ausencia.setMotivoRechazo(motivo);
			updateAusencia(ausencia);
		} else {
			throw new RechazoNoValidoException(ausencia);
		}
	}
}
