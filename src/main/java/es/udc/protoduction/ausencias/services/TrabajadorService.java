/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.services;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.services.exceptions.AusenciaNoValidaException;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import java.util.List;

/**
 *
 * @author diana.yanez
 */
public interface TrabajadorService {

    public List<Trabajador> findAll();

    public void saveTrabajador(Trabajador t);

    public Trabajador findById(long trabajadorId);

    public List<Proyecto> getProjectsByWorkerId(long id)
            throws InstanceNotFoundException;

    public void updateDiasAusencia(Ausencia ausencia) throws AusenciaNoValidaException;
}
