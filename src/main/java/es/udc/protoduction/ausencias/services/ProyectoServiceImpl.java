package es.udc.protoduction.ausencias.services;

import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.repositories.ProyectoDao;
import es.udc.protoduction.ausencias.repositories.TrabajadorDao;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("proyectoService")
public class ProyectoServiceImpl implements ProyectoService {

    @Autowired
    private ProyectoDao projectDao;
    @Autowired
    private TrabajadorDao workerDao;

    @Override
    public List<Proyecto> getProjectByWorkerId(long id)
            throws InstanceNotFoundException {
        Trabajador worker = workerDao.find(id);
        if (worker == null) {
            throw new InstanceNotFoundException(id, Trabajador.class.getName());
        } else {
            return new ArrayList<>(worker.getProjects());
        }
    }

    @Override
    public List<Proyecto> getAllProjects() {
        return this.projectDao.findAll();
    }

    @Override
    public List<Trabajador> getWorkersByProjectId(long id) throws InstanceNotFoundException {
        Proyecto pro = projectDao.find(id);
        if (pro != null) {
            return new ArrayList<>(pro.getWorkers());
        } else {
            throw new InstanceNotFoundException(id, Proyecto.class.getName());
        }
    }

    @Override
    public void setWorkers(long idPro, Set<Long> listaTrabajadores) throws InstanceNotFoundException {
        Proyecto pro = projectDao.find(idPro);
        if (pro != null) {
            List<Trabajador> toDelete = new ArrayList<>();
            // No se borran directamente porque java.util.ConcurrentModificationException
            for (Trabajador iter : pro.getWorkers()) {
                if (!listaTrabajadores.contains(iter.getId())) {
                    toDelete.add(iter);
                }
            }

            for (Trabajador iter : toDelete) {
                pro.removeWorker(iter);
            }

            // Se añaden los trabajadores en listaTrabajadores que no esten en el proyecto
            for (Long iter : listaTrabajadores) {
                Trabajador t = workerDao.find(iter);
                if (!pro.getWorkers().contains(t)) {
                    pro.addWorker(t);
                }
            }
            projectDao.update(pro);
        } else {
            throw new InstanceNotFoundException(idPro, Proyecto.class.getName());
        }
    }
}
