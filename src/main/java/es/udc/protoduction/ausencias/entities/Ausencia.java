package es.udc.protoduction.ausencias.entities;

import es.udc.protoduction.ausencias.util.AusenciasState;
import es.udc.protoduction.ausencias.util.AusenciasType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
public class Ausencia {

    public final static byte VACACION = 0;
    public final static byte BAJA = 1;
    private long id;
    private String descripcion;
    private Calendar fechaIni;
    private Calendar fechaFin;
    private byte tipoAusencia;
    private byte estado;
    private Trabajador trabajador;
    private String motivo;
    private String motivoRechazo;
    private Boolean vistaPorEncargado;

    public Ausencia() {
    }

    public Ausencia(long id, String descripcion, Calendar fechaIni, Calendar fechaFin,
            byte tipoAusencia, byte estado, Trabajador trabajador) {
        this.id = id;
        this.descripcion = descripcion;
        this.fechaIni = fechaIni;
        this.fechaFin = fechaFin;
        this.tipoAusencia = tipoAusencia;
        this.estado = estado;
        this.trabajador = trabajador;
        this.vistaPorEncargado = false;
        this.motivoRechazo = "";
    }

    @Column(name = "Id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "Descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "Fecha_ini")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Calendar getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Calendar fechaIni) {
        this.fechaIni = fechaIni;
    }

    @Column(name = "Fecha_fin")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Calendar getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Calendar fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Column(name = "Tipo_ausencia")
    public byte getTipoAusencia() {
        return tipoAusencia;
    }

    public void setTipoAusencia(byte tipoAusencia) {
        this.tipoAusencia = tipoAusencia;
    }

    @Column(name = "Estado")
    public byte getEstado() {
        return estado;
    }

    public void setEstado(byte estado) {
        this.estado = estado;
    }

    @ManyToOne
    @JoinColumn(name = "Trabajador_id")
    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    @Column(name = "Motivo")
    public String getMotivo() {
        return this.motivo;
    }
    
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
  
    
    @Column(name = "MotivoRechazo")
    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    @Column(name = "Vista")
    public Boolean isVistaPorEncargado() {
        return vistaPorEncargado;
    }

    public void setVistaPorEncargado(Boolean vistaPorEncargado) {
        this.vistaPorEncargado = vistaPorEncargado;
    }

    @Override
    public String toString() {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return AusenciasType.valueOf(tipoAusencia).toString() + " " + formatter.format(fechaIni.getTime());
    }
}
