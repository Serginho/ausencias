package es.udc.protoduction.ausencias.services;

import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import java.util.List;
import java.util.Set;

public interface ProyectoService {

    List<Proyecto> getProjectByWorkerId(long id)
            throws InstanceNotFoundException;

    List<Trabajador> getWorkersByProjectId(long id) throws InstanceNotFoundException;

    void setWorkers(long idPro, Set<Long> listaTrabajadores) throws InstanceNotFoundException;

    List<Proyecto> getAllProjects();
}
