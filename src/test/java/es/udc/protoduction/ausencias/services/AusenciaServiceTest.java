package es.udc.protoduction.ausencias.services;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.repositories.AusenciaDao;
import es.udc.protoduction.ausencias.repositories.ProyectoDao;
import es.udc.protoduction.ausencias.repositories.TrabajadorDao;
import es.udc.protoduction.ausencias.services.exceptions.AusenciaNoValidaException;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import es.udc.protoduction.ausencias.util.AusenciasState;
import es.udc.protoduction.ausencias.util.AusenciasType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author david
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml",
    "classpath:spring-config-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class AusenciaServiceTest {

    private final Calendar TODAY;

    public AusenciaServiceTest() {
        TODAY = Calendar.getInstance();
        TODAY.set(Calendar.HOUR_OF_DAY, 0);
        TODAY.set(Calendar.MINUTE, 0);
        TODAY.set(Calendar.SECOND, 0);
        TODAY.set(Calendar.MILLISECOND, 0);
        TODAY.getTimeInMillis();
    }

    private Calendar fromToday(int days) {
        Calendar fromToday = Calendar.getInstance();
        fromToday.setTimeInMillis(TODAY.getTimeInMillis());
        fromToday.add(Calendar.DAY_OF_MONTH, days);
        return fromToday;
    }

    @Autowired
    private AusenciaDao ausenciaDao;

    @Autowired
    private TrabajadorDao trabajadorDao;

    @Autowired
    private AusenciaService ausenciaService;

    @Autowired
    private ProyectoDao proyectoDao;

    @Test
    public void testSaveAusencia() throws InstanceNotFoundException, AusenciaNoValidaException {
        Trabajador t = new Trabajador(0, "Paco", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t);
        Calendar ini = Calendar.getInstance();
        Calendar fin = Calendar.getInstance();
        ini.set(2015, 5, 1);
        fin.set(2015, 5, 3);
        Ausencia a = ausenciaService.saveAusencia(ini, fin, AusenciasType.BAJA, AusenciasState.PENDIENTE, "ausencia", t.getId());
        Assert.assertEquals(a, ausenciaDao.find(a.getId()));
        Assert.assertTrue(t.getAusencias().contains(a));
    }

    @Test(expected = AusenciaNoValidaException.class)
    public void testSaveAusenciaFechasCruzadas() throws AusenciaNoValidaException, InstanceNotFoundException {
        Trabajador t = new Trabajador(0, "Paco", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t);
        Calendar ini = Calendar.getInstance();
        Calendar fin = Calendar.getInstance();
        ini.set(2015, 5, 1);
        fin.set(2015, 4, 3);
        Ausencia a = ausenciaService.saveAusencia(ini, fin, AusenciasType.BAJA, AusenciasState.PENDIENTE, "ausencia", t.getId());
    }

    @Test(expected = AusenciaNoValidaException.class)
    public void testSaveAusenciaFechaPasado() throws AusenciaNoValidaException, InstanceNotFoundException {
        Trabajador t = new Trabajador(0, "Paco", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t);
        Calendar ini = Calendar.getInstance();
        Calendar fin = Calendar.getInstance();
        ini.set(2011, 5, 1);
        fin.set(2015, 4, 3);
        Ausencia a = ausenciaService.saveAusencia(ini, fin, AusenciasType.BAJA, AusenciasState.PENDIENTE, "ausencia", t.getId());
    }

    @Test(expected = AusenciaNoValidaException.class)
    public void testSaveAusenciaNoSuficientesDias() throws AusenciaNoValidaException, InstanceNotFoundException {
        Trabajador t = new Trabajador(0, "Paco", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t);
        Calendar ini = Calendar.getInstance();
        Calendar fin = Calendar.getInstance();
        ini.set(2014, 5, 1);
        fin.set(2019, 4, 3);
        Ausencia a = ausenciaService.saveAusencia(ini, fin, AusenciasType.VACACIONES, AusenciasState.PENDIENTE, "ausencia", t.getId());
    }

    @Test
    public void testFindAll() {
        Trabajador t = new Trabajador(0, "Paco", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t);
        Ausencia a = new Ausencia(0, "ausencia", Calendar.getInstance(), Calendar.getInstance(), AusenciasType.VACACIONES.getValue(), AusenciasState.PENDIENTE.getValue(), t);
        ausenciaDao.create(a);
        Ausencia aa = new Ausencia(0, "ausencia", Calendar.getInstance(), Calendar.getInstance(), AusenciasType.BAJA.getValue(), AusenciasState.PENDIENTE.getValue(), t);
        ausenciaDao.create(aa);

        Set<Ausencia> expected = new HashSet<>();
        expected.add(aa);
        expected.add(a);

        Set<Ausencia> actual = new HashSet(ausenciaService.findAll());
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFindByWorkerId() throws Exception {
        Trabajador t = new Trabajador(0, "Paco", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t);
        Ausencia a = new Ausencia(0, "ausencia", Calendar.getInstance(), Calendar.getInstance(), AusenciasType.VACACIONES.getValue(), AusenciasState.PENDIENTE.getValue(), t);
        ausenciaDao.save(a);
        Ausencia aa = new Ausencia(0, "ausencia", Calendar.getInstance(), Calendar.getInstance(), AusenciasType.BAJA.getValue(), AusenciasState.PENDIENTE.getValue(), t);
        ausenciaDao.save(aa);
        t.getAusencias().add(a);
        t.getAusencias().add(aa);
        trabajadorDao.update(t);

        Set<Ausencia> expected = new HashSet<>();
        expected.add(aa);
        expected.add(a);

        Set<Ausencia> actual = new HashSet<>(ausenciaService.findByWorkerId(t.getId()));
        Assert.assertEquals(expected, actual);
    }

    /*
     * Mínimo de trabajadores del proyecto: 2
     * Situación de las ausencias aceptadas:
     * _______| 1 | 2 | 3 | 4 | 5 | 6 |
     *     t1 | - | - | x | x | - | - |
     *   boss | x | x | - | - | x | x |
     * Esperado: No se puede aceptar ausencia ningún día.
     */
    @Test
    public void testNoEsValida() throws AusenciaNoValidaException, InstanceNotFoundException {
        final int testInterval = 6;

        Trabajador subject = new Trabajador(0, "subject", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(subject);

        Trabajador boss = new Trabajador(0, "Boss", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(boss);

        Trabajador t1 = new Trabajador(0, "t1", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t1);

        Proyecto p = new Proyecto(0, "proyecto", fromToday(30), boss, 2);
        p.addWorker(boss);
        p.addWorker(t1);
        p.addWorker(subject);
        proyectoDao.create(p);

        // WORKER |   1   |  2  |  3  |  4  |  5  |  6  |
        //  t1    |   -   |  -  |  x  |  x  |  -  |  -  |
        //  boss  |   x   |  x  |  -  |  -  |  x  |  x  |
        ausenciaService.saveAusencia(fromToday(1), fromToday(2), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "t1", t1.getId());
        ausenciaService.saveAusencia(fromToday(5), fromToday(6), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "t1", t1.getId());
        ausenciaService.saveAusencia(fromToday(3), fromToday(4), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "boss", boss.getId());

        List<Proyecto> expected = new ArrayList<>();
        expected.add(p);

        for (int i = 1; i <= testInterval; i++) {
            for (int j = i; j <= testInterval; j++) {
                System.out.println("Ausencia NoEsValida betweeen days " + String.valueOf(i) + " and " + String.valueOf(j));
                Ausencia ausencia = new Ausencia(0, "subject" + String.valueOf(i) + String.valueOf(j),
                        fromToday(i), fromToday(j), AusenciasType.VACACIONES.getValue(),
                        AusenciasState.PENDIENTE.getValue(), subject);
                ausenciaDao.create(ausencia);
                Assert.assertEquals(expected, ausenciaService.esValida(ausencia.getId()));
                ausenciaDao.delete(ausencia);
            }
        }
    }

    /*
     * Mínimo de trabajadores de los proyectos: 1
     * Situación de las ausencias aceptadas por proyecto:
     *  P1    |   1   |  2  |  3  |  4  |  5  |  6  |
     * bossP1 |   x   |  x  |  -  |  -  |  x  |  x  |

     *  P1    |   1   |  2  |  3  |  4  |  5  |  6  |
     * bossP2 |   -   |  -  |  x  |  x  |  -  |  -  |
     * Esperado: No se puede aceptar ausencia ningún día.
     *           Se devuelve alguno de los proyectos en conflicto.
     */
    @Test
    public void testNoEsValidaAlgunoVariosProyectos() throws AusenciaNoValidaException, InstanceNotFoundException {
        final int testInterval = 6;

        Trabajador subject = new Trabajador(0, "subject", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(subject);

        Trabajador bossP1 = new Trabajador(0, "BossP1", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(bossP1);

        Trabajador bossP2 = new Trabajador(0, "BossP2", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(bossP2);

        Proyecto p1 = new Proyecto(0, "proyecto", fromToday(30), bossP1, 1);
        p1.addWorker(bossP1);
        p1.addWorker(subject);
        proyectoDao.create(p1);

        Proyecto p2 = new Proyecto(0, "proyecto", fromToday(30), bossP2, 1);
        p2.addWorker(bossP2);
        p2.addWorker(subject);
        proyectoDao.create(p2);

        //  P1    |   1   |  2  |  3  |  4  |  5  |  6  |
        // bossP1 |   x   |  x  |  -  |  -  |  x  |  x  |
        //
        //  P1    |   1   |  2  |  3  |  4  |  5  |  6  |
        // bossP2 |   -   |  -  |  x  |  x  |  -  |  -  |
        ausenciaService.saveAusencia(fromToday(1), fromToday(2), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "BossP1", bossP1.getId());
        ausenciaService.saveAusencia(fromToday(5), fromToday(6), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "BossP1", bossP1.getId());
        ausenciaService.saveAusencia(fromToday(3), fromToday(4), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "BossP2", bossP2.getId());

        Set<Proyecto> expected = new HashSet<>();
        expected.add(p1);
        expected.add(p2);

        for (int i = 1; i <= testInterval; i++) {
            for (int j = i; j <= testInterval; j++) {
                System.out.println("Ausencia NoEsValidaVariosProyectos betweeen days " + String.valueOf(i) + " and " + String.valueOf(j));
                Ausencia ausencia = new Ausencia(0, "subject" + String.valueOf(i) + String.valueOf(j),
                        fromToday(i), fromToday(j), AusenciasType.VACACIONES.getValue(),
                        AusenciasState.PENDIENTE.getValue(), subject);
                ausenciaDao.create(ausencia);
                Assert.assertTrue(expected.containsAll(ausenciaService.esValida(ausencia.getId())));
                ausenciaDao.delete(ausencia);
            }
        }
    }

    /*
     * Mínimo de trabajadores de los proyectos: 1
     * Situación de las ausencias aceptadas por proyecto:
     *  P1    |   1   |  2  |  3  |  4  |  5  |  6  |
     * bossP1 |   -   |  -  |  -  |  -  |  -  |  -  |

     *  P1    |   1   |  2  |  3  |  4  |  5  |  6  |
     * bossP2 |   -   |  -  |  -  |  -  |  -  |  -  |
     * Esperado: No se puede aceptar ausencia ningún día.
     *           Se devuelven todos los proyectos en conflicto.
     */
    @Test
    public void testNoEsValidaVariosProyectos() throws AusenciaNoValidaException, InstanceNotFoundException {
        final int testInterval = 6;

        Trabajador subject = new Trabajador(0, "subject", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(subject);

        Trabajador bossP1 = new Trabajador(0, "BossP1", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(bossP1);

        Trabajador bossP2 = new Trabajador(0, "BossP2", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(bossP2);

        Proyecto p1 = new Proyecto(0, "proyecto", fromToday(30), bossP1, 1);
        p1.addWorker(bossP1);
        p1.addWorker(subject);
        proyectoDao.create(p1);

        Proyecto p2 = new Proyecto(0, "proyecto", fromToday(30), bossP2, 1);
        p2.addWorker(bossP2);
        p2.addWorker(subject);
        proyectoDao.create(p2);

        ausenciaService.saveAusencia(fromToday(1), fromToday(testInterval), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "BossP1", bossP1.getId());
        ausenciaService.saveAusencia(fromToday(1), fromToday(testInterval), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "BossP2", bossP2.getId());

        Set<Proyecto> expected = new HashSet<>();
        expected.add(p1);
        expected.add(p2);

        for (int i = 1; i <= testInterval; i++) {
            for (int j = i; j <= testInterval; j++) {
                System.out.println("Ausencia NoEsValidaVariosProyectos betweeen days " + String.valueOf(i) + " and " + String.valueOf(j));
                Ausencia ausencia = new Ausencia(0, "subject" + String.valueOf(i) + String.valueOf(j),
                        fromToday(i), fromToday(j), AusenciasType.VACACIONES.getValue(),
                        AusenciasState.PENDIENTE.getValue(), subject);
                ausenciaDao.create(ausencia);
                Set<Proyecto> actual = new HashSet<>(ausenciaService.esValida(ausencia.getId()));
                Assert.assertTrue(expected.containsAll(actual));
                Assert.assertTrue(actual.containsAll(expected));
                ausenciaDao.delete(ausencia);
            }
        }
    }

    /*
     * El único trabajador del proyecto pide una ausencia.
     * Mínimo de trabajadores del proyecto: 1
     * Situación de las ausencias aceptadas por proyecto:
     * Esperado: No se puede crear ausencia ningún día.
     */
    @Test
    public void testEsValidaUnicoTrabajador() throws AusenciaNoValidaException, InstanceNotFoundException {
        final int testInterval = 6;

        Trabajador subject = new Trabajador(0, "subject", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(subject);

        Proyecto p1 = new Proyecto(0, "proyecto", fromToday(30), subject, 1);
        p1.addWorker(subject);
        proyectoDao.create(p1);

        List<Proyecto> expected = new ArrayList<>();
        expected.add(p1);

        for (int i = 1; i <= testInterval; i++) {
            for (int j = i; j <= testInterval; j++) {
                System.out.println("Ausencia ValidaVariosProyectos betweeen days " + String.valueOf(i) + " and " + String.valueOf(j));
                Ausencia ausencia = new Ausencia(0, "subject" + String.valueOf(i) + String.valueOf(j),
                        fromToday(i), fromToday(j), AusenciasType.VACACIONES.getValue(),
                        AusenciasState.PENDIENTE.getValue(), subject);
                ausenciaDao.create(ausencia);
                Assert.assertEquals(expected, ausenciaService.esValida(ausencia.getId()));
                ausenciaDao.delete(ausencia);
            }
        }
    }

    /*
     * Mínimo de trabajadores de los proyectos: 1
     * Situación de las ausencias aceptadas por proyecto:
     *  P1    |   1   |  2  |  3  |  4  |  5  |  6  |
     *  tP1   |   x   |  x  |  x  |  x  |  x  |  x  |
     * bossP1 |   x   |  x  |  -  |  -  |  x  |  x  |


     *  P2    |   1   |  2  |  3  |  4  |  5  |  6  |
     *  tP1   |   x   |  x  |  x  |  x  |  x  |  x  |
     * bossP2 |   -   |  -  |  x  |  x  |  -  |  -  |
     * Esperado: Se puede crear ausencia cualquier día.
     */
    @Test
    public void testEsValidaVariosProyectos() throws AusenciaNoValidaException, InstanceNotFoundException {
        final int testInterval = 6;

        Trabajador subject = new Trabajador(0, "subject", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(subject);

        Trabajador bossP1 = new Trabajador(0, "BossP1", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(bossP1);
        Trabajador tP1 = new Trabajador(0, "tP1", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(tP1);

        Trabajador bossP2 = new Trabajador(0, "BossP2", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(bossP2);
        Trabajador tP2 = new Trabajador(0, "tP1", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(tP2);

        Proyecto p1 = new Proyecto(0, "proyecto", fromToday(30), bossP1, 1);
        p1.addWorker(bossP1);
        p1.addWorker(tP1);
        p1.addWorker(subject);
        proyectoDao.create(p1);

        Proyecto p2 = new Proyecto(0, "proyecto", fromToday(30), bossP2, 1);
        p2.addWorker(bossP2);
        p2.addWorker(tP2);
        p2.addWorker(subject);
        proyectoDao.create(p2);

        //  P1    |   1   |  2  |  3  |  4  |  5  |  6  |
        //  tP1   |   x   |  x  |  x  |  x  |  x  |  x  |
        // bossP1 |   x   |  x  |  -  |  -  |  x  |  x  |
        //
        //  P2    |   1   |  2  |  3  |  4  |  5  |  6  |
        //  tP2   |   x   |  x  |  x  |  x  |  x  |  x  |
        // bossP2 |   -   |  -  |  x  |  x  |  -  |  -  |
        ausenciaService.saveAusencia(fromToday(1), fromToday(2), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "BossP1", bossP1.getId());
        ausenciaService.saveAusencia(fromToday(5), fromToday(6), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "BossP1", bossP1.getId());
        ausenciaService.saveAusencia(fromToday(3), fromToday(4), AusenciasType.BAJA,
                AusenciasState.ACEPTADA, "BossP2", bossP2.getId());

        for (int i = 1; i <= testInterval; i++) {
            for (int j = i; j <= testInterval; j++) {
                System.out.println("Ausencia ValidaVariosProyectos betweeen days " + String.valueOf(i) + " and " + String.valueOf(j));
                Ausencia ausencia = new Ausencia(0, "subject" + String.valueOf(i) + String.valueOf(j),
                        fromToday(i), fromToday(j), AusenciasType.VACACIONES.getValue(),
                        AusenciasState.PENDIENTE.getValue(), subject);
                ausenciaDao.create(ausencia);
                Assert.assertTrue(ausenciaService.esValida(ausencia.getId()).isEmpty());
                ausenciaDao.delete(ausencia);
            }
        }
    }
}
