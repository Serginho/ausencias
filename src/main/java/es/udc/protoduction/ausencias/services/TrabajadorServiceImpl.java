/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.services;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.repositories.ProyectoDao;
import es.udc.protoduction.ausencias.repositories.TrabajadorDao;
import es.udc.protoduction.ausencias.services.exceptions.AusenciaNoValidaException;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import es.udc.protoduction.ausencias.util.AusenciasModelUtil;
import es.udc.protoduction.ausencias.util.AusenciasType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("trabajadorService")
public class TrabajadorServiceImpl implements TrabajadorService {

    @Autowired
    private TrabajadorDao trabajadorDao;
    @Autowired
    private ProyectoDao projectDao;

    /**
     * Crea un nuevo caledario con solo la fecha
     *
     * @param calendar calendario de referencia
     * @return nueva instancia de calendar, solo con fecha
     */
    private Calendar clearTime(Calendar calendar) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(calendar.getTimeInMillis());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.getTimeInMillis(); // No borrar! recalcula milisegundos despues de los set
        return c;
    }

    @Override
    public List<Trabajador> findAll() {
        return trabajadorDao.findAll();
    }

    @Override
    public void saveTrabajador(Trabajador t) {
        trabajadorDao.create(t);
    }

    @Override
    public Trabajador findById(long trabajadorId) {
        return trabajadorDao.find(trabajadorId);
    }

    @Override
    public List<Proyecto> getProjectsByWorkerId(long id) throws InstanceNotFoundException {
        Trabajador t = trabajadorDao.find(id);
        if (t == null) {
            throw new InstanceNotFoundException(id, Trabajador.class.getName());
        } else {
            return new ArrayList(t.getProjects());
        }
    }

    @Override
    public void updateDiasAusencia(Ausencia ausencia) throws AusenciaNoValidaException {
        AusenciasModelUtil.verificarAusencia(ausencia);

        long milisAusencia = (clearTime(ausencia.getFechaFin()).getTimeInMillis() - clearTime(ausencia.getFechaIni()).getTimeInMillis());
        long diasAusencia = milisAusencia / AusenciasModelUtil.ONE_DAY + 1; // Los dias de inicio y fin de la ausencia incluidos
        Trabajador t = ausencia.getTrabajador();
        if (ausencia.getTipoAusencia() != AusenciasType.BAJA.getValue()) {
            if (t.getRemainingHolidays() - (int) diasAusencia >= 0) {
                t.setRemainingHolidays(t.getRemainingHolidays() - (int) diasAusencia);
                t.setEnjoyedHolidays(t.getEnjoyedHolidays() + (int) diasAusencia);
            } else {
                throw new AusenciaNoValidaException(ausencia, "No le quedan días para pedir");
            }
        }
        trabajadorDao.update(t);
    }
}
