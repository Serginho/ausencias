/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.util;

/**
 *
 * @author diana.yanez
 */
public enum AusenciasType {

    VACACIONES((byte) 0),
    BAJA((byte) 1),
    OTROS((byte) 2);

    private final byte value;

    private AusenciasType(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return this.value;
    }

    public static AusenciasType valueOf(byte b) {
        for (AusenciasType iter : AusenciasType.values()) {
            if (b == iter.getValue()) {
                return iter;
            }
        }
        throw new RuntimeException("The byte value: " + String.valueOf(b)
                + " does not match with any AusenciasType");
    }
}
