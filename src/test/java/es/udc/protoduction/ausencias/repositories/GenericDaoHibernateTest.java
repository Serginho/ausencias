package es.udc.protoduction.ausencias.repositories;

import es.udc.protoduction.ausencias.entities.Trabajador;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author david
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml", "classpath:spring-config-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class GenericDaoHibernateTest {

    @Autowired
    private TrabajadorDao trabajadorDao;

    public TrabajadorDao getTrabajadorDao() {
        return trabajadorDao;
    }

    public void setTrabajadorDao(TrabajadorDao trabajadorDao) {
        this.trabajadorDao = trabajadorDao;
    }

    private Trabajador getValidTrabajador(String name) {
        return new Trabajador(0, name, "surname", 0, 0, Calendar.getInstance());
    }

    @Test
    public void testCreateAndFind() {
        Trabajador t = getValidTrabajador("Leroy");
        trabajadorDao.create(t);
        Assert.assertEquals(t, trabajadorDao.find(t.getId()));
    }

    @Test
    public void testUpdate() {
        Trabajador t = getValidTrabajador("Leroy");
        trabajadorDao.create(t);
        t.setName("Jenkins");
        trabajadorDao.update(t);
        Assert.assertEquals("Jenkins", trabajadorDao.find(t.getId()).getName());
    }

    @Test
    public void testSave() {
        Trabajador t = getValidTrabajador("Leroy");
        trabajadorDao.create(t);
        Assert.assertEquals(t, trabajadorDao.find(t.getId()));

        t.setName("Jenkins");
        trabajadorDao.update(t);
        Assert.assertEquals("Jenkins", trabajadorDao.find(t.getId()).getName());
    }

    @Test
    public void testDelete() {
        Trabajador t = getValidTrabajador("Leroy");
        trabajadorDao.create(t);
        trabajadorDao.delete(t);
        Assert.assertNull(trabajadorDao.find(t.getId()));
    }

    @Test
    public void testFindAll() {
        Trabajador t = getValidTrabajador("Leroy");
        Trabajador tr = getValidTrabajador("Jenkins");
        trabajadorDao.create(t);
        trabajadorDao.create(tr);

        List<Trabajador> expected = new ArrayList<>();
        expected.add(t);
        expected.add(tr);

        Assert.assertEquals(expected, trabajadorDao.findAll());
    }

}
