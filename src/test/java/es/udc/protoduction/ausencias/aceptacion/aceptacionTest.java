/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.udc.protoduction.ausencias.aceptacion;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.entities.Proyecto;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.repositories.AusenciaDao;
import es.udc.protoduction.ausencias.repositories.ProyectoDao;
import es.udc.protoduction.ausencias.repositories.TrabajadorDao;
import es.udc.protoduction.ausencias.services.AusenciaService;
import es.udc.protoduction.ausencias.services.exceptions.AusenciaNoValidaException;
import es.udc.protoduction.ausencias.services.exceptions.InstanceNotFoundException;
import es.udc.protoduction.ausencias.util.AusenciasState;
import es.udc.protoduction.ausencias.util.AusenciasType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kaworu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml",
    "classpath:spring-config-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class aceptacionTest {

    @Autowired
    private TrabajadorDao trabajadorDao;
    @Autowired
    private AusenciaService ausenciaService;
    @Autowired
    private AusenciaDao ausenciaDao;
    @Autowired
    private ProyectoDao projectDao;

    @Test
    public void pruebaAceptacion() throws InstanceNotFoundException, AusenciaNoValidaException {
        Calendar fechaFin = Calendar.getInstance();
        fechaFin.set(2016, 8, 10);
        Trabajador t = new Trabajador(0, "José", "Garcia", 10, 5, fechaFin);
        trabajadorDao.create(t);
        Calendar ini = Calendar.getInstance();
        Calendar fin = Calendar.getInstance();
        ini.set(2014, 12, 2);
        fin.set(2014, 12, 5);
        Ausencia a = ausenciaService.saveAusencia(ini, fin, AusenciasType.VACACIONES, AusenciasState.PENDIENTE, "ausencia", t.getId());
        //+++++++++++++++
        Trabajador t2 = new Trabajador(1, "Ana", "Gomez", 10, 5, fechaFin);
        trabajadorDao.create(t2);
        ini.set(2014, 12, 2);
        fin.set(2014, 12, 3);
        a = ausenciaService.saveAusencia(ini, fin, AusenciasType.BAJA, AusenciasState.PENDIENTE, "ausencia", t2.getId());
        //+++++++++++++++
        Trabajador t3 = new Trabajador(2, "Luis", "Fernandez", 3, 5, fechaFin);
        trabajadorDao.create(t3);
        ini.set(2014, 12, 3);
        fin.set(2014, 12, 3);
        a = ausenciaService.saveAusencia(ini, fin, AusenciasType.VACACIONES, AusenciasState.PENDIENTE, "ausencia", t3.getId());

        //----------------
        Trabajador t4 = new Trabajador(3, "Jefe", "Proyecto", 3, 5, fechaFin);
        trabajadorDao.create(t4);
        fechaFin.set(2014, 12, 22);
        Proyecto p = new Proyecto(0, "p1", fechaFin, t, 3);
        projectDao.create(p);
        Trabajador t5 = new Trabajador(4, "Trabajador", "de relleno", 3, 5, fechaFin);
        trabajadorDao.create(t5);
        t.addProject(p);
        t2.addProject(p);
        t3.addProject(p);
        t4.addProject(p);
        t5.addProject(p);
        trabajadorDao.update(t);
        trabajadorDao.update(t2);
        trabajadorDao.update(t3);
        trabajadorDao.update(t4);
        trabajadorDao.update(t5);
        //El administrador debería encontrarse 3 ausencias pendientes
        assertEquals(3, ausenciaService.findAllPending().size());

        List<Ausencia> listaAus = ausenciaService.findByWorkerId(t2.getId());
        listaAus.get(0).setEstado(AusenciasState.ACEPTADA.getValue());
        //La baja debería notificarse como aceptada
        assertEquals(1, ausenciaService.findAcceptedByWorkerId(t2.getId()).size());

        listaAus = ausenciaService.findByWorkerId(t3.getId());
        listaAus.get(0).setEstado(AusenciasState.ACEPTADA.getValue());

        listaAus = ausenciaService.findByWorkerId(t.getId());
        //Al validar la úlima, se detecta que es inválida
        List<Proyecto> expected = new ArrayList<>();
        expected.add(p);
        assertEquals(expected, ausenciaService.esValida(listaAus.get(0).getId()));
        listaAus.get(0).setEstado(AusenciasState.RECHAZADA.getValue());
        listaAus.get(0).setMotivo("Demasiadas bajas");
    }
}
