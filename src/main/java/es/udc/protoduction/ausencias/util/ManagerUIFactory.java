package es.udc.protoduction.ausencias.util;

import es.udc.protoduction.ausencias.ui.GestionAusenciasEncargado;
import es.udc.protoduction.ausencias.ui.ManagerUI;
import es.udc.protoduction.ausencias.ui.VentanaGestor;
import es.udc.protoduction.ausencias.ui.VentanaTrabajadores;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author david
 */
public class ManagerUIFactory {

    private static ApplicationContext context;
    private static ManagerUI managerUI;
    private static VentanaGestor ventanaGestor;
    private static VentanaTrabajadores ventanaTrabajadores;
    private static GestionAusenciasEncargado ventanaAusencias;

    private static ApplicationContext getContext() {
        if (context == null) {
            context = new ClassPathXmlApplicationContext("spring-config.xml");
        }
        return context;
    }

    public static ManagerUI getManagerUI() {
        if (managerUI == null) {
            managerUI = getContext().getBean(ManagerUI.class);
        }
        return managerUI;
    }

    public static VentanaGestor getVentanaGestor() {
        if (ventanaGestor == null) {
            ventanaGestor = getContext().getBean(VentanaGestor.class);
        }
        return ventanaGestor;
    }

    public static VentanaTrabajadores getVentanaTrabajadores() {
        if (ventanaTrabajadores == null) {
            ventanaTrabajadores = getContext().getBean(VentanaTrabajadores.class);
        }
        return ventanaTrabajadores;
    }
    
    public static GestionAusenciasEncargado getVentanaAusencias() {
        if (ventanaAusencias == null) {
            ventanaAusencias = getContext().getBean(GestionAusenciasEncargado.class);
        }
        return ventanaAusencias;
    }
}
