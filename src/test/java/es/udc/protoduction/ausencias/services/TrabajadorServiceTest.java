package es.udc.protoduction.ausencias.services;

import es.udc.protoduction.ausencias.entities.Ausencia;
import es.udc.protoduction.ausencias.entities.Trabajador;
import es.udc.protoduction.ausencias.repositories.AusenciaDao;
import es.udc.protoduction.ausencias.repositories.TrabajadorDao;
import es.udc.protoduction.ausencias.util.AusenciasState;
import es.udc.protoduction.ausencias.util.AusenciasType;
import java.util.Calendar;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author david
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml",
    "classpath:spring-config-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class TrabajadorServiceTest {

    @Autowired
    TrabajadorDao trabajadorDao;
    @Autowired
    AusenciaDao ausenciaDao;
    @Autowired
    TrabajadorService trabajadorService;

    @Test
    public void testUpdateDiasAusencia() throws Exception {
        Trabajador t = new Trabajador(0, "Paco", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t);
        Calendar fechaIni = Calendar.getInstance();
        fechaIni.add(Calendar.DAY_OF_MONTH, 7);
        Calendar fechaFin = Calendar.getInstance();
        fechaFin.add(Calendar.DAY_OF_MONTH, 8);
        Ausencia a = new Ausencia(0, "Vacaciones", fechaIni, fechaFin, AusenciasType.VACACIONES.getValue(), AusenciasState.ACEPTADA.getValue(), t);
        ausenciaDao.create(a);
        trabajadorService.updateDiasAusencia(a);
        Assert.assertEquals(8, t.getRemainingHolidays());
    }

    @Test
    public void testUpdateDiasAusenciaOneDay() throws Exception {
        Trabajador t = new Trabajador(0, "Paco", "Garcia", 10, 5, Calendar.getInstance());
        trabajadorDao.create(t);
        Calendar fechaIni = Calendar.getInstance();
        fechaIni.add(Calendar.DAY_OF_MONTH, 7);
        Calendar fechaFin = Calendar.getInstance();
        fechaFin.add(Calendar.DAY_OF_MONTH, 7);
        Ausencia a = new Ausencia(0, "Vacaciones", fechaIni, fechaFin, AusenciasType.VACACIONES.getValue(), AusenciasState.ACEPTADA.getValue(), t);
        ausenciaDao.create(a);
        trabajadorService.updateDiasAusencia(a);
        Assert.assertEquals(9, t.getRemainingHolidays());
    }

}
